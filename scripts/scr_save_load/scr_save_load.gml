function save_highscore(highscore){
	var save_data = {
		highscore: highscore
	}
	
	var data_string = json_stringify(save_data);
	var buffer = buffer_create(string_byte_length(data_string) + 1, buffer_fixed, 1);
	buffer_write (buffer, buffer_string, data_string);
	buffer_save(buffer, "savegame.save");
	buffer_delete(buffer);
	
	show_debug_message("game saved" + data_string);
}

function load_highscore(){
	if(file_exists("savegame.save")){
		var buffer = buffer_load("savegame.save");
		var data_string = buffer_read(buffer, buffer_string);
		buffer_delete(buffer);	
		var load_data = json_parse(data_string);
		
		return load_data.highscore
	}
}