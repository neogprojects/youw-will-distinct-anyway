function draw_game_over_text(){
	draw_set_halign(fa_center);
	draw_set_valign(fa_center);
	draw_set_font(fnt_game_over);
	
	draw_text(800, 300, "GAME OVER");
}