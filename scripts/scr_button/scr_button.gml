function get_hover() {
	var _mouseX = device_mouse_x_to_gui(0);
	var _mouseY = device_mouse_y_to_gui(0);

	return point_in_rectangle(_mouseX, _mouseY, x, y, x + width, y + height);
}

function create_button(button, _x, _y, _width, _height, _text) {
	var _button = instance_create_layer(_x, _y, "GUI", button);

	with (_button) {
		width = _width;
		height = _height;
		text = _text;
	}

	return _button;
}
