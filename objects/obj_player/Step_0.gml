key_right = keyboard_check(ord("D"));
key_left = -keyboard_check(ord("A"));
key_down = keyboard_check(ord("S"));
key_jump = keyboard_check_pressed(vk_space);

if (controlls){
	move = key_right + key_left;
	hsp = move_speed * move
	vsp = vsp + player_gravity;
}

check_vertival_colision = place_meeting(x, y + sign(vsp), obj_wall);

if (place_meeting(x, y + 1, obj_wall) && key_jump && controlls){
	vsp = -jump_height;
}

if (key_down){
	player_gravity = 2;
} else {
	player_gravity = 1;
}

if (place_meeting(x + hsp, y, obj_wall)){
	while (!place_meeting(x+sign(hsp), y, obj_wall)){
		x = x + sign(hsp);
	}
	hsp = 0;
}

if (place_meeting(x, y + vsp, obj_wall)){
	while (!place_meeting(x, y + sign(vsp), obj_wall)){
		y = y + sign(vsp);
	}
	vsp = 0;
}

x = x + hsp;
y = y + vsp;


if(!check_vertival_colision){
	sprite_index = spr_player_jump;
	image_speed = 0;
	
	if (vsp < 0){
		image_index = 0;
	} else {
		image_index = 1;
	}
} else {
	image_speed = 1;
	if (hsp == 0){
		sprite_index = spr_player_idle;
	} else {
		sprite_index = spr_player_run;
	}
	
}

if (hsp != 0) image_xscale = sign(hsp);













