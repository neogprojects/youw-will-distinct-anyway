global.lives -= 1;

if (global.lives <= 0){
	instance_create_layer(x, y, "game_layer", obj_new_camera_follower);
	controlls = false;
	hsp = 0;
	vsp = 0;
	self.visible = false;
	if (!destroyed){
		instance_create_layer(x, y, "game_layer", obj_player_body);
		instance_create_layer(x, y, "game_layer", obj_player_leg);
		instance_create_layer(x, y, "game_layer", obj_player_leg);
		instance_create_layer(x, y, "game_layer", obj_player_arm);
		instance_create_layer(x, y, "game_layer", obj_player_arm);
		instance_create_layer(x, y, "game_layer", obj_player_head);
		destroyed = true
	}
	
}

instance_destroy(other);
instance_create_layer(x, y, "game_layer", obj_comet_shard);
instance_create_layer(x, y, "game_layer", obj_comet_shard);
instance_create_layer(x, y, "game_layer", obj_comet_shard);
instance_create_layer(x, y, "game_layer", obj_comet_shard);

if (global.score > global.highscore){
	save_highscore(global.score);
}