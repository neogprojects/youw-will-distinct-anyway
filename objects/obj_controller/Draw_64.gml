draw_set_halign(fa_left);
draw_set_valign(fa_top);
draw_set_font(fnt_game_text);
draw_text(10, 10, "Score: " + string(global.score));
draw_text(200, 10, "Highscore: " + string(global.highscore));
scr_draw_lives(global.lives);

if(global.lives <= 0){
	global.game_over = true;
	draw_game_over_text();
}