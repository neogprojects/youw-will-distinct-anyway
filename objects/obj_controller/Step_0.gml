global.score = floor(global.game_speed_score/6);
comet_counter_1 -= 1;
comet_counter_2 -= 1;
comet_counter_3 -= 1;
comet_counter_4 -= 1;
time_to_reduce_spawn_time -= 1;

if (time_to_reduce_spawn_time <= 0) {
	minimum_spawn_time -= 1;
	maximum_spawn_time -= 3;
	time_to_reduce_spawn_time = 600;
	global.lives += 1;
}

if (comet_counter_1 < 0){
	instance_create_layer(irandom_range(0, 10000), -400, "game_layer", obj_comet);
	comet_counter_1 = irandom_range(minimum_spawn_time,maximum_spawn_time);
}

if (comet_counter_2 < 0){
	instance_create_layer(irandom_range(0, 10000), -400, "game_layer", obj_comet);
	comet_counter_2 = irandom_range(minimum_spawn_time,maximum_spawn_time);
}

if (comet_counter_3 < 0){
	instance_create_layer(irandom_range(0, 10000), -400, "game_layer", obj_comet);
	comet_counter_3 = irandom_range(minimum_spawn_time,maximum_spawn_time);
}

if (comet_counter_4 < 0){
	instance_create_layer(irandom_range(0, 10000), -400, "game_layer", obj_comet);
	comet_counter_4 = irandom_range(minimum_spawn_time,maximum_spawn_time);
}

if(global.lives > 0){
	global.game_speed_score += 1;
}


