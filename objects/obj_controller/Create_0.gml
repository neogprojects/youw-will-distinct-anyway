comet_counter_1 = 0;
comet_counter_2 = 0;
comet_counter_3 = 0;
comet_counter_4 = 0;

global.lives = 3;
global.game_speed_score = 0;
global.score = 0;
global.game_over = false;

if(!file_exists("savegame.save")){
	global.highscore =  0;
} else {
	global.highscore = load_highscore();
}

time_to_reduce_spawn_time = 600;

minimum_spawn_time = 10;
maximum_spawn_time = 20;