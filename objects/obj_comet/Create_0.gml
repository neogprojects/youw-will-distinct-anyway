comet_gravity = 4;
hsp = irandom_range(-4, 4);
randomize();
partical_system = part_system_create();
part_system_depth(partical_system, depth + 10);
schweif = part_type_create();

var comet_red = make_color_rgb(191, 51, 0);
var comet_orrange = make_color_rgb(213, 92, 0)
var commet_yellow = make_color_rgb(213, 185, 0);

part_type_sprite(schweif, comet_particels, 0, 0, 0);
part_type_color3(schweif, comet_red, comet_orrange, commet_yellow);
part_type_alpha2(schweif, 1, 0);
part_type_life(schweif, 30, 60);