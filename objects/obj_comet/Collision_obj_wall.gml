if (other.y == 576) {
	instance_destroy(self);
	instance_create_layer(x, y, "game_layer", obj_comet_shard);
	instance_create_layer(x, y, "game_layer", obj_comet_shard);
	instance_create_layer(x, y, "game_layer", obj_comet_shard);
	instance_create_layer(x, y, "game_layer", obj_comet_shard);
}
if (point_distance(x, y, obj_player.x, obj_player.y) < 800){
	audio_play_sound(snd_explosion, 1, 0);
	for (var i = 0; i < 3; i++){
		camera_set_view_pos(
		global.cam, 
		global.cam_x + irandom_range(-5, 5), 
		global.cam_y + irandom_range(-5, 5));
	}
	
}

