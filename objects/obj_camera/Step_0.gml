global.cam_x = lerp(follow.x, helper.x, 0.3) - view_width_half;
global.cam_y = follow.y - view_height_half - 100;

global.cam_x = clamp(global.cam_x, global.cam_x, room_width-view_width_half);
global.cam_y = clamp(global.cam_y, global.cam_y, room_height-view_height_half);

camera_set_view_pos(
	global.cam, 
	global.cam_x, 
	global.cam_y);
	
if(keyboard_check_pressed(ord("G"))){
	for (var i = 0; i < 3; i++){
		camera_set_view_pos(
		global.cam, 
		global.cam_x + irandom_range(-5, 5), 
		global.cam_y + irandom_range(-5, 5));
	}
}
	
if (global.lives <= 0){
	follow = obj_new_camera_follower;
}